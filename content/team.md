+++
template = "team.html"

[[extra.members]]
name = "Kostis Karantias"
institution = "IOHK"
github = "gtklocker"
twitter = "gtklocker"

[[extra.members]]
name = "Andrianna Polydouri"
institution = "University of Athens"
github = "andripol"

[[extra.members]]
name = "Stelios Daveas"
institution = "University of Athens"
github = "sdaveas"

[[extra.members]]
name = "Dionysis Zindros"
institution = "University of Athens"
github = "dionyziz"
twitter = "dionyziz"

[[extra.members]]
name = "Aggelos Kiayias"
institution = "University of Edinburgh & IOHK"
github = "solegga"
twitter = "sol3gga"

[[extra.members]]
name = "Orestis Konstantinidis"
institution = "University of Athens"
github = "orestkon"

[[extra.members]]
name = "Roman Brunner"
institution = "ETH Zürich"
github = "jogli5er"

[[extra.members]]
name = "Zeta Avarikioti"
institution = "ETH Zürich"
github = "zetavar"

[[extra.members]]
name = "Christos Nasikas"
institution = "Elastic"
github = "cnasikas"
+++